# SocketConnector

## Prerequirements

To launch the device server *Hostname* and *port* properties must be specified.

## Running

To run the server please type the following command:

```./Socket [device name]```

The executable can be found in the main filesystem tree under /Socket inside docker container.
