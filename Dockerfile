FROM centos:7
COPY maxiv.repo /etc/yum.repos.d/
RUN cd /usr/local/bin && \
    curl -sO https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh && \
    chmod +x wait-for-it.sh
RUN yum install -y epel-release && yum clean all
RUN yum install -y libtango9-devel tango-java && yum clean all
RUN yum install -y make gcc-c++ && yum clean all

COPY . /SocketConnector
WORKDIR /SocketConnector/SocketConnector
RUN make
COPY / ./compiled
